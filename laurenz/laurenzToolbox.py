import pandas as pd
import json
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.decomposition import PCA
from sklearn.neighbors import LocalOutlierFactor
import scipy.stats as stats
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest

from pylab import *
import matplotlib.colors

def get_outliners(Dataframe, features, pca_components, outliner_algorithm = 'LOF'):
    df_feature = features

    df_standardized = StandardScaler().fit_transform(df_feature)
    pca = PCA(n_components=pca_components)
    components = pca.fit_transform(df_standardized)
    components = pd.DataFrame(data=components)

    if outliner_algorithm == 'LOF' :
        lof = LocalOutlierFactor(n_neighbors=500, contamination='auto')
        lof_anomalies = lof.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(lof_anomalies)):
            if(lof_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)

        anomalies_idizes = Dataframe[anomalies_index]
        df_N_W_no_anomalies = Dataframe.drop(columns=anomalies_idizes)

        return [sum(lof_anomalies==-1),anomalies_index, df_N_W_no_anomalies, components]  
    
    if outliner_algorithm == 'IF':
        clf = IsolationForest(max_samples='auto')
        clf.fit(components)
        clf_anomalies = clf.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(clf_anomalies)):
            if(clf_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)


        anomalies_idizes = Dataframe[anomalies_index]
        df_N_W_no_anomalies = Dataframe.drop(columns=anomalies_idizes)
        return [sum(clf_anomalies==-1),anomalies_index, df_N_W_no_anomalies, components]
    
    raise Exception('Unkown Algorithm')
    
    
def create_outliner_plot_2D(Dataframe, features, outliner_algorithm = 'LOF'):

    df_feature = features

    df_standardized = StandardScaler().fit_transform(df_feature)
    pca = PCA(n_components=2)
    components = pca.fit_transform(df_standardized)
    components = pd.DataFrame(data=components)

    
    if outliner_algorithm == 'LOF' :
        
        lof = LocalOutlierFactor(n_neighbors=500, contamination='auto')
        lof_anomalies = lof.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(lof_anomalies)):
            if(lof_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)

        no_anomalies_index = []
        for i in range(Dataframe.shape[1]):
            if i not in anomalies_index:
                no_anomalies_index.append(i)
                
        anomalies_idizes = Dataframe[anomalies_index]
        df_N_W_no_anomalies = Dataframe.drop(columns=anomalies_idizes)

        
        
        fig = plt.figure()

        x_scores =  lof.negative_outlier_factor_

        outliner_scatter =plt.scatter(components.iloc[anomalies_index,0],components.iloc[anomalies_index,1],color="Red",  s=2)
        non_outline_scatter =plt.scatter(components.iloc[no_anomalies_index,0],components.iloc[no_anomalies_index,1],color="Green",  s=2)

        radius = (x_scores.max() - x_scores) / (x_scores.max() - x_scores.min())
#         plt.scatter(components.iloc[:, 0], components.iloc[:, 1], s=1000 * radius, edgecolors='r',
#                     facecolors='none', label='Outlier scores',alpha = 0.1)


        plt.legend([outliner_scatter, non_outline_scatter],
                   ["outliner", "non outliner"],
                   loc="upper left")
        plt.title("Local Outlier Factor (LOF)")
        plt.show()
        return
    
    if outliner_algorithm == 'IF':
        
        clf = IsolationForest(max_samples='auto')
        clf.fit(components)
        clf_anomalies = clf.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(clf_anomalies)):
            if(clf_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)


        no_anomalies_index = []
        for i in range(Dataframe.shape[1]):
            if i not in anomalies_index:
                no_anomalies_index.append(i)

                        
        xx, yy = np.meshgrid(np.linspace(components[0].min() -1, components[0].max() +1, 110), 
                             np.linspace(components[1].min() -1, components[1].max() +1, 110))
        Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)


        fig = plt.figure()
        plt.contourf(xx, yy, Z, cmap=plt.cm.Blues_r)


        outliner_scatter =plt.scatter(components.iloc[anomalies_index,0],components.iloc[anomalies_index,1],color="Red",  s=1)
        non_outline_scatter =plt.scatter(components.iloc[no_anomalies_index,0],components.iloc[no_anomalies_index,1],color="Green",  s=1)

        plt.legend([outliner_scatter, non_outline_scatter],
                   ["outliner", "non outliner"],
                   loc="upper left")
        plt.title("IsolationForest")

        plt.show()
        return
        
    raise Exception('Unkown Algorithm')
    
    
def create_outliner_plot_3D(Dataframe, features, outliner_algorithm = 'LOF'):
    df_feature =features
    
    df_standardized = StandardScaler().fit_transform(df_feature)
    pca = PCA(n_components=3)
    components = pca.fit_transform(df_standardized)
    components = pd.DataFrame(data=components)
    
    if outliner_algorithm == 'LOF' :
        
        lof = LocalOutlierFactor(n_neighbors=500, contamination='auto')
        lof_anomalies = lof.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(lof_anomalies)):
            if(lof_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)

        no_anomalies_index = []
        for i in range(Dataframe.shape[1]):
            if i not in anomalies_index:
                no_anomalies_index.append(i)
                
        anomalies_idizes = Dataframe[anomalies_index]
        df_N_W_no_anomalies = Dataframe.drop(columns=anomalies_idizes)

        fig = plt.figure()

        ax = fig.add_subplot(projection='3d')
        outliner_scatter = ax.scatter(components.iloc[anomalies_index,0],
                                      components.iloc[anomalies_index,1],
                                      components.iloc[anomalies_index,2],color= "red", s=1)
        non_outline_scatter = ax.scatter(components.iloc[no_anomalies_index,0],
                                         components.iloc[no_anomalies_index,1],
                                         components.iloc[no_anomalies_index,2],color = "green", s=1)
        plt.legend([outliner_scatter, non_outline_scatter],
                   ["outliner", "non outliner"],
                   loc="upper left")
        plt.title("Local Outlier Factor (LOF)")

        plt.show()
        
        return
    
    if outliner_algorithm == 'IF':
        
        clf = IsolationForest(max_samples='auto')
        clf.fit(components)
        clf_anomalies = clf.fit_predict(components)

        anomalies_index = []
        for eintrag in range(len(clf_anomalies)):
            if(clf_anomalies[eintrag] == -1):
                anomalies_index.append(eintrag)


        no_anomalies_index = []
        for i in range(Dataframe.shape[1]):
            if i not in anomalies_index:
                no_anomalies_index.append(i)


        fig = plt.figure()

        ax = fig.add_subplot(projection='3d')
        outliner_scatter = ax.scatter(components.iloc[anomalies_index,0],
                                      components.iloc[anomalies_index,1],
                                      components.iloc[anomalies_index,2],color= "red", s=1)
        non_outline_scatter = ax.scatter(components.iloc[no_anomalies_index,0],
                                         components.iloc[no_anomalies_index,1],
                                         components.iloc[no_anomalies_index,2],color = "green", s=1)
        plt.legend([outliner_scatter, non_outline_scatter],
                   ["outliner", "non outliner"],
                   loc="upper left")
        plt.title("IsolationForest (IF)")

        plt.show()
        return
        
    raise Exception('Unkown Algorithm')